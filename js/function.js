$(function () {

    "use stric";
    // Scroll Top 

    var scrollButton = $("#scroll-top");

    $(window).scroll(function () {

        if ($(this).scrollTop() >= 150) {
            scrollButton.show();
        } else {
            scrollButton.hide();
        }

        if ($(this).scrollTop() >= 42) {
            $('.bootsnav').addClass('navbar-fixed-top ');
        } else {
            $('.bootsnav').removeClass('navbar-fixed-top ');

        }

    });


    // scroll top

    scrollButton.click(function () {

        $("html,body").animate({
            scrollTop: 0
        }, 1500);
    });

    // loading

    $(window).on('load', function () {
        if ($('.loading').length) {
            $('.loading').delay(200).fadeOut(500);
        }
    });

    // change bars

    $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function () {
        $(this).toggleClass('open');
    });


// bloger
$(".blog-owl").owlCarousel({
    items: 4,
    autoplay: 2000,
    loop: true,
    nav: false,
    dots: true,
    rtl:true,
    autoplayHoverPause: true,
    margin:30,
    navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
    responsive: {
        300: {
            items: 1
        },
        600: {
            items: 2
        },
        900: {
            items: 3
        }, 
        1200: {
            items:3
        }
    }
});[]



    // remove collapse in mobile

    $('.navbar.navbar-default .navs').click(function () {
        $('.navbar.navbar-default .navbar-collapse').removeClass('in');
        $('.navbar-header .navbar-toggle').removeClass('open');
    });


    // remove active panel
    $('#accordion-1').on('show.bs.collapse', function (n) {
        $(n.target).siblings('.panel-heading').toggleClass('active');
    });
    $('#accordion-1').on('hide.bs.collapse', function (n) {
        $(n.target).siblings('.panel-heading').toggleClass('active');
    });

    // picture
    $(".owl_collection").owlCarousel({
        items: 1,
        autoplay: 4000,
        rtl: true,
        loop: true,
        nav: false,
        margin: 30,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            }
            ,
            768: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });

    // accordion
    $('#accordion-1').on('show.bs.collapse', function (n) {
        $(n.target).siblings('.panel-heading').find('.panel-title i').toggleClass('fa-angle-up fa-angle-down');
    });
    $('#accordion-1').on('hide.bs.collapse', function (n) {
        $(n.target).siblings('.panel-heading').find('.panel-title i').toggleClass('fa-angle-down fa-angle-up');
    });

    // Trriger mixItUp

    // $("#popular").mixItUp();

    // // Active Ul Li In Porto

    // $(".popular .select li").click(function () {

    //     $(this).addClass("selected").siblings().removeClass("selected");
    // });


    // main slider
    var tpj = jQuery;
    var revapi8;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_8_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_8_1");
        } else {
            revapi8 = tpj("#rev_slider_8_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "../../revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 9000,

                particles: {  
                    startSlide:  "fidfrst",
                    endSlide: "last",
                    zIndex: "1",
                    particles: {
                        number: {
                            value: 300
                        },
                        color: {
                            value: "#cad2dd"
                        },
                        shape: {
                            type: "none",
                            stroke: {
                                width: 2,
                                color: "#ffffff",
                                opacity: 1
                            },
                            image: {
                                src: ""
                            }
                        },
                        opacity: {
                            value: 0.1,
                            random: false,
                            min: 0.25,
                            anim: {
                                enable: true,
                                speed: 1,
                                opacity_min: 0,
                                sync: false
                            }
                        },
                        size: {
                            value: 1,
                            random: true,
                            min: 0.5,
                            anim: {
                                enable: false,
                                speed: 30,
                                size_min: 1,
                                sync: false
                            }
                        },
                        line_linked: {
                            enable: true,
                            distance: 100,
                            color: "#fff",
                            opacity: 0.35,
                            width: 0.75
                        },
                        move: {
                            enable: true,
                            speed: 1,
                            direction: "top",
                            random: true,
                            min_speed: 2,
                            straight: false,
                            out_mode: "out"
                        }
                    },
                    interactivity: {
                        events: {
                            onhover: {
                                enable: true,
                                mode: "grab"
                            },
                            onclick: {
                                enable: false,
                                mode: "bubble"
                            }
                        },
                        modes: {
                            grab: {
                                distance: 250,
                                line_linked: {
                                    opacity: 0.5
                                }
                            },
                            bubble: {
                                distance: 400,
                                size: 100,
                                opacity: 1
                            },
                            repulse: {
                                distance: 75
                            }
                        }
                    }
                },
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%"
                },
                parallax: {
                    type: "mouse+scroll",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [100, 2, 3, 20, 25, 30, 35, 40, 45, 50, 46, 47, 48, 49, 50, 505],
                    type: "mouse+scroll",
                    disable_onmobile: "on"
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1024, 778, 480],
                gridheight: [600, 500, 450, 350],
                lazyType: "smart",
                shadow: 0,
                spinner: "spinner3",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }

        RsParticlesAddOn(revapi8);
    }); /*ready*/



    // single category
    if ($(window).width() > 1150) {
        $('#example5').sliderPro({
            width: '100%',
            height: 424,
            orientation: 'horizontal',
            loop: false,
            arrows: true,
            responsive: true,
            imageScaleMode: 'contain',
            buttons: false,
            thumbnailsPosition: 'right',
            thumbnailPointer: true,
            thumbnailWidth: 80,
        });
    } else {
        $('#example5').sliderPro({
            width: '100%',
            height: 424,
            orientation: 'horizontal',
            loop: false,
            arrows: true,
            responsive: true,
            imageScaleMode: 'contain',
            buttons: false,
            thumbnailWidth: 0,
        });

    }


});